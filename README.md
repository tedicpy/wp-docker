# wp-docker para Tedic

Plantilla para crear WordPress usando docker

# Cómo utilizar

**1.** Clonar este repo:
```
git clone git@gitlab.com:tedicpy/wp-docker.git nuevo-docker
```

**2.** Copiar los archivos de configuracion desde los ejemplos
```
cd nuevo-docker
cp customphp.ini-sample customphp.ini
cp .env-example .env
```
y editar **ambos archivos** colocando todo lo necesario

**3.** Levantar
```
docker compose up -d
```
